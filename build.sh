#!/bin/bash

HTTP_JOB_NAME="feedback-android-sdk"

########################################
# Internal variables

HTTP_BUILD_NUMBER="%build.number%"
SERVER_URL="https://by.originally.us/jenkins_post.php"
UPLOAD_COUNT=0
BUILD_ID=""
BUILD_CHANGELOG=""

########################################################################
# Standard initialization
########################################################################

# directory of this script
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
SCRIPT_NAME="$(basename $BASH_SOURCE)"


########################################
## Delete all previous APKs
#for i in `find . -name "*.apk" -type f`; do
#    rm -v "${i}"
#done

# Install Java 17 automatically
switchToJava17(){
    echo "========= Checking Java 17 environment variables ========="

    # Should NOT use the plugin version Java; will throw error "Could not find tools.jar" during gradle build command
    # https://stackoverflow.com/a/68278517
    JAVA_PLUGIN_PATH="/Library/Internet Plug-Ins/JavaAppletPlugin.plugin/Contents/Home"
    if [ -d "$JAVA_PLUGIN_PATH" ]; then
        echo "   Deleting Java Plugin in ${JAVA_PLUGIN_PATH}"
        sudo rm -rf "$JAVA_PLUGIN_PATH"
    fi

    # List all JDK found in this machine
    /usr/libexec/java_home -V

    # Force using Java 17
    unset JAVA_HOME
    export JAVA_HOME=`/usr/libexec/java_home -v 17`

    # Make sure it really exists
    if [ ! -d "$JAVA_HOME" ]; then
        echo "❌  JDK 17 (Java 17) is not installed. Trying to install Java 17 (JDK 17) via Homebrew..."
        brew update
        #brew install openjdk
        #sudo ln -sfn /opt/homebrew/opt/openjdk/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk
        echo "⌨️  Please enter sudo password to link JDK 17 (Java 17)"
        brew install openjdk@17
        sudo ln -sfn /opt/homebrew/opt/openjdk@17/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk

        export JAVA_HOME=`/usr/libexec/java_home -v 17`

        # Make sure it really exists after installation
        if [ ! -d "$JAVA_HOME" ]; then
            echo "❌  JDK 17 (Java 17) still not found. Please install it manually."
            exit 1
        fi
    fi

    # Make sure it is not Java 1.8 (JDK 8)
    SUB='1.8'
    if [[ "$JAVA_HOME" == *"$SUB"* ]]; then
        echo "❌  Java 8 is not compatible. Trying to install Java 17 (JDK 17) via Homebrew..."
        brew update
        #brew install openjdk
        #sudo ln -sfn /opt/homebrew/opt/openjdk/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk
        brew install openjdk@17
        echo "⌨️  Please enter sudo password to link JDK 17 (Java 17)"
        sudo ln -sfn /opt/homebrew/opt/openjdk@17/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk

        export JAVA_HOME=`/usr/libexec/java_home -v 17`
        sudo ln -sfn /opt/homebrew/opt/openjdk/libexec/openjdk.jdk /Library/Java/JavaVirtualMachines/openjdk.jdk

        # Make sure it really exists after installation
        if [ ! -d "$JAVA_HOME" ]; then
            echo "❌  JDK 17 (Java 17) still not found. Please install it manually."
            exit 1
        fi
    fi

    echo "JAVA_HOME = $JAVA_HOME"
}

# Try to fix Android env variables automatically
checkAndroidRequiredEnvironmentVariables(){
    echo "========= Checking Android environment variables ========="

    # Note: Gradle Plugin doesn't work with Java 8
    switchToJava17

    # Note: ANDROID_HOME is deprecated, replaced by ANDROID_SDK_ROOT
    if [ -z "$ANDROID_SDK_ROOT" ]; then
        echo "⚠️  'ANDROID_SDK_ROOT' is not defined!! Automatically configured with default value."
        export ANDROID_SDK_ROOT=$HOME/Library/Android/sdk
    fi

    echo "ANDROID_SDK_ROOT = $ANDROID_SDK_ROOT"

    # Check exists
    if [ ! -d "$JAVA_HOME" ]; then
        echo "❌  JDK not found in JAVA_HOME = $JAVA_HOME. Please install it manually"
        exit 1
    fi

    # Check exists
    if [ ! -d "$ANDROID_SDK_ROOT" ]; then
        echo "❌  Android SDK not found in ANDROID_SDK_ROOT = $ANDROID_SDK_ROOT. Please install Android Studio manually"
        exit 1
    fi

    # Just to be sure
    # Mainly for sdkmanager in tools/bin
    export PATH="${PATH}:${ANDROID_SDK_ROOT}/emulator"
    export PATH="${PATH}:${ANDROID_SDK_ROOT}/platform-tools"

    # Prefer newer sdkmanager from cmdline-tools
    if [ -d ${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin ]; then
        export PATH="${PATH}:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin"
    else
        export PATH="${PATH}:${ANDROID_SDK_ROOT}/tools/bin"
    fi

    # Make sure sdkmanager command is executable
    if ! sdkmanager --list ; then
        echo "which sdkmanager: $(which sdkmanager)"
        echo "❌  Failed to execute 'sdkmanager'. Please send the build log to developer."
        exit 1
    fi

    # Install the latest stuff that we need
    echo "========= Installing 'platform-tools' ========="
    yes | sdkmanager "platform-tools"

    #echo "========= Installing 'build-tools;32.0.0' ========="
    #yes | sdkmanager "build-tools;32.0.0"

    # Cordova does not automatically pick up latest build-tools https://stackoverflow.com/a/71043857
    # We install 30.0.3 just to be sure
    echo "========= Installing 'build-tools;30.0.3' ========="
    yes | sdkmanager "build-tools;30.0.3"

    echo "========= Installing 'platforms;android-32' ========="
    yes | sdkmanager "platforms;android-32"

#    echo "========= Installing 'ndk;21.0.6113669' ========="
#    yes | sdkmanager "ndk;21.0.6113669"

    echo "========= Accept all licenses ========="
    yes | sdkmanager --licenses

    echo "========= sdkmanager installed ========="
    sdkmanager --list | awk '/Installed/{flag=1; next} /Available/{flag=0} flag'
}

# Check homebrew installed
checkHomeBrewInstalled(){
    echo "========= Checking 'brew' is installed ========="
    if ! brew --version > /dev/null 2>&1; then
        #echo "'homebrew' is not installed! To install visit: https://brew.sh/"
        #exit 1
        echo "'homebrew' is not installed! Trying to install automatically..."
        yes '' | /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

        #export new environment variables
        echo "$(/opt/homebrew/bin/brew shellenv)"
        eval "$(/opt/homebrew/bin/brew shellenv)"

        #double check again after installation
        if ! brew --version > /dev/null 2>&1; then
            echo "'homebrew' failed to be installed automatically. To install manually use the following command from https://brew.sh/"
            echo "    /bin/bash -c \"$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)\"  "
            exit 1
        fi
    fi
}

# Check known issue with homebrew folder permission
checkHomeBrewUsrLocalPermission(){
    HOMEBREW_PREFIX_FOLDER=$(brew --prefix)

    #not found, do nothing
    if [ -z "$HOMEBREW_PREFIX_FOLDER" ]; then
        return 0
    fi
    if [ ! -f "$HOMEBREW_PREFIX_FOLDER" ]; then
        return 0
    fi

    echo "========= Checking 'Homebrew' permission ========="
    OWNER=$(stat -f '%Su' $HOMEBREW_PREFIX_FOLDER)
    if [ "$OWNER" == "$(whoami)" ]; then
        return
    fi

    #https://stackoverflow.com/questions/40592463/homebrew-install-fails-while-copying-files/46516733
    echo "⚠️  /usr/local/Homebrew folder should not be owned by 'root' user. This is a known issue with Homebrew."
    echo "   More Info: https://stackoverflow.com/questions/40592463/homebrew-install-fails-while-copying-files/46516733"
    echo "   Please enter sudo password to fix this automatically"
    echo "   sudo chown -R $(whoami) $HOMEBREW_PREFIX_FOLDER"
    sudo chown -R $(whoami) $HOMEBREW_PREFIX_FOLDER

    OWNER=$(stat -f '%Su' $HOMEBREW_PREFIX_FOLDER)
    if [ "$OWNER" != "$(whoami)" ]; then
        echo "❌  Failed to update permission for '$HOMEBREW_PREFIX_FOLDER! To fix this manually, use the following command:"
        echo "   sudo chown -R $(whoami) $HOMEBREW_PREFIX_FOLDER"
        exit 1
    fi
}

checkHomeBrewPluginInstalled(){
  FORMULA=$1
  echo "========= Checking homebrew package '$FORMULA' is installed ========="
        if ! jq --version > /dev/null 2>&1; then
            echo "⚠️  '$FORMULA' is not installed! Installing..."
            if ! brew install $FORMULA > /dev/null 2>&1; then
                echo "❌  Failed to install '$FORMULA'. Please install it manually with the following command:"
                echo "    brew install $FORMULA"
                exit 1
            fi
        fi

        echo "========= Upgrading homebrew package '$FORMULA' ========="
        if ! brew upgrade $FORMULA > /dev/null 2>&1; then
            echo "⚠️  Failed to upgrade '$FORMULA'. Ignored."
        fi
}

checkRubyGemInstalled(){
    FORMULA=$1
    echo "========= Checking ruby gem '$FORMULA' is installed ========="
    if ! gem list -i $FORMULA 2>&1; then
        echo "⚠️  gem '$FORMULA' is not installed! Installing..."
        gem install --user-install $FORMULA
    fi

    echo "========= Updating ruby gem '$FORMULA' ========="
    if ! gem update $FORMULA > /dev/null 2>&1; then
        echo "⚠️  Failed to update gem '$FORMULA'. Ignored."
    fi
}

checkRubyEnvironment(){
    echo "========= Setting up Rubxy environment with RVM ========="

    #Don't need GEM_HOME when using RVM
    unset GEM_HOME

    #Install latest RVM, not Ruby
    brew install gnupg
    gpg --keyserver hkp://keyserver.ubuntu.com --recv-keys 409B6B1796C275462A1703113804BB82D39DC0E3 7D2BAF1CF37B13E2069D6956105BD0E739499BDB
    curl -sSL https://get.rvm.io | bash
    rvm get stable --auto-dotfiles
    rvm --version

    #Show list of installed Ruby versions via RVM
    echo
    rvm list

    # Get the current Ruby version
    CURRENT_RUBY_VERSION=$(ruby -v | grep -o '[0-9]\+\.[0-9]\+\.[0-9]\+')
    SELECTED_RVM_VERSION="3.3.0"

    # Check if the current Ruby version is not the target version
    if [ "$CURRENT_RUBY_VERSION" != "$SELECTED_RVM_VERSION" ]
    then
        echo "Current Ruby version is $CURRENT_RUBY_VERSION, which is not $SELECTED_RVM_VERSION."
        echo "Automatically install Ruby $SELECTED_RVM_VERSION via RVM..."
        # Example command: update Ruby version or any other command
        # rbenv install 3.3.0
        # rbenv global 3.3.0

        #Explicitly tell RVM where the OpenSSL v1.1 is located (installed via homebrew)
        #Note: incompatible with openssl@3
        #https://github.com/rvm/rvm/issues/5379
        rvm reinstall 3.3.0 --with-openssl-dir=$(brew --prefix openssl@1.1) --with-readline-dir=$(brew --prefix readline) --with-libyaml-dir=$(brew --prefix libyaml) --disable-dtrace --disable-docs

        #Fix error: RVM is not a function, selecting rubies with 'rvm use ...' will not work.
        source ~/.rvm/scripts/rvm

        #Select Ruby version to use temporarily, don't set as default
        rvm use $SELECTED_RVM_VERSION
    fi

    #should be the RVM ruby. (To see if there is no conflict with OSX`s ruby)
    echo "which ruby: $(which ruby)"
    echo "ruby --version: $(ruby --version)"

    #debug
    echo "which gem: $(which gem)"
    echo "gem --version: $(gem --version)"

    #gem install cocoapods
    gem pristine --all

    # the scope of current ruby gems
    echo "gem query --local"
    gem query --local
}

# Delete all previous build files, if any
cleanOldBuilds(){
    #Handle space character in filename
    oIFS=$IFS
    IFS=$'\n'

    echo "========= Cleaning old builds ========="
    for i in `find . -name "build" -type d`; do
        echo "Deleteting $i"
        rm -rf "${i}"
    done
    for i in `find . -name "*.ipa" -type f`; do
        rm -rf "${i}"
    done
    for i in `find . -name "*.apk" -type f`; do
        rm -rf "${i}"
    done
    for i in `find . -name "*.aab" -type f`; do
        rm -rf "${i}"
    done

    IFS=$oIFS
}

# Upload IPA files to server
uploadIPAs(){
    #Handle space character in filename
    oIFS=$IFS
    IFS=$'\n'

    for i in `find . -name "*.ipa" -type f`; do
        echo "Uploading ${i}..."
        curl -F "file=@${i}" \
                -F "HTTP_JOB_NAME=${HTTP_JOB_NAME}" \
                -F "HTTP_BUILD_NUMBER=${HTTP_BUILD_NUMBER}" \
                ${SERVER_URL}
                ((UPLOAD_COUNT=UPLOAD_COUNT+1))
    done

    IFS=$oIFS
}

# Upload APK files to server
uploadAPKs(){
    #Handle space character in filename
    oIFS=$IFS
    IFS=$'\n'

    for i in `find . -name "*.apk" -type f`; do
        echo "Uploading ${i}..."
        curl -F "file=@${i}" \
            -F "HTTP_JOB_NAME=${HTTP_JOB_NAME}" \
            -F "HTTP_BUILD_NUMBER=${HTTP_BUILD_NUMBER}" \
            ${SERVER_URL}
        ((UPLOAD_COUNT=UPLOAD_COUNT+1))
    done

    IFS=$oIFS
}

# Upload AAB files to server
uploadAABs(){
    #Handle space character in filename
    oIFS=$IFS
    IFS=$'\n'

    for i in `find ./app/build/outputs -name "*.aab" -type f`; do
        echo "Uploading ${i}..."
        curl -F "file=@${i}" \
            -F "HTTP_JOB_NAME=${HTTP_JOB_NAME}" \
            -F "HTTP_BUILD_NUMBER=${HTTP_BUILD_NUMBER}" \
            ${SERVER_URL}
        ((UPLOAD_COUNT=UPLOAD_COUNT+1))
    done

    IFS=$oIFS
}

#Fail TeamCity build if there's no files uploaded
failIfNoUploads() {
    if [ "$UPLOAD_COUNT" -le "0" ]; then
        echo "##teamcity[buildStatus status='FAILURE' text='No files were uploaded. Check previous build steps']"
        exit 1
    fi
}

generateBuildChangelog(){
  echo  "=========== Generate Build Changelog ==========="
  file="$TEAMCITY_BUILD_PROPERTIES_FILE"
  if [ -f "$file" ]
  then
    echo "Found: $file"

    while IFS='=' read -r key value
    do
      key=$(echo $key | tr '.' '_')
      eval ${key}=\${value}
    done < "$file"

    BUILD_ID=${teamcity_build_id}
  else
    echo "$file not found."
  fi

  echo "BUILD_ID: $BUILD_ID"

  result=$( curl -s -H "Accept: application/json" -H "Authorization: Bearer $TC_API_ACCESS_TOKEN" "$TC_BASE_URL/app/rest/changes?locator=build:(id:$BUILD_ID)&fields=count,change:(version,username,date,comment)" | jq -r '.change' | sed 's/\\[tn]//g')
  BUILD_CHANGELOG=$(echo "$result" | jq -r '.[] | "- " + .comment')
  echo "$BUILD_CHANGELOG"

  export CHANGELOG="$BUILD_CHANGELOG"
}

########################################################################
# Build procedure
########################################################################

checkAndroidRequiredEnvironmentVariables

checkHomeBrewInstalled
checkHomeBrewUsrLocalPermission
#checkHomeBrewPluginInstalled 'jq'
#checkHomeBrewPluginInstalled 'fastlane'
#checkRubyEnvironment
#checkRubyGemInstalled 'fastlane-plugin-firebase_app_distribution'
#
#generateBuildChangelog

# Install gems in Gemfile
# always remove Gemfile.lock in order to use the latest version of fastlane & cocoapods
#echo "========= bundle install ========="
#echo "pwd: $(pwd)"
#unset GEM_HOME
#
#rm -rf $SCRIPT_DIR/Gemfile.lock
#if [ -e Gemfile.lock ]
#then
#    echo "Gemfile.lock still existed!"
#else
#    echo "Removed Gemfile.lock"
#fi
#
#bundle install

# Run fastlane via bundler/Gemfile support
# https://docs.fastlane.tools/getting-started/ios/setup/#use-a-gemfile
echo "========= Building APKs ========="

cleanOldBuilds

./gradlew clean assembleDebug --stacktrace
uploadAPKs

failIfNoUploads
