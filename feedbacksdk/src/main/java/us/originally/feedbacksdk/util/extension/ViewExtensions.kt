package us.originally.feedbacksdk.util.extension

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.res.TypedArray
import android.graphics.Color
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.SystemClock
import android.text.Html
import android.text.method.LinkMovementMethod
import android.util.AttributeSet
import android.util.TypedValue
import android.view.MotionEvent
import android.view.View
import android.view.ViewTreeObserver
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.ColorRes
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import com.bumptech.glide.load.MultiTransformation
import com.bumptech.glide.load.resource.bitmap.BitmapTransformation
import kotlinx.coroutines.*
import us.originally.feedbacksdk.R
import us.originally.feedbacksdk.util.GlideApp
import us.originally.feedbacksdk.util.GlideRequest

internal fun Activity.hideKeyboardOnTapOutside() {
    if (currentFocus != null) {
        (getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)?.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }
}

@SuppressLint("ClickableViewAccessibility")
internal fun View.setBlinkClickAnimation(): View {
    return this.apply {
        setOnTouchListener { v, event ->
            when (event.action) {
                MotionEvent.ACTION_DOWN -> if (this.isEnabled) this.alpha = 0f
                MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> this.alpha = 1f
            }
            return@setOnTouchListener false
        }
    }
}

internal inline fun <T : View> T.afterMeasured(crossinline f: T.() -> Unit) {
    viewTreeObserver.addOnGlobalLayoutListener(object : ViewTreeObserver.OnGlobalLayoutListener {
        override fun onGlobalLayout() {
            if (measuredWidth > 0 && measuredHeight > 0) {
                viewTreeObserver.removeOnGlobalLayoutListener(this)
                f()
            }
        }
    })
}

internal fun AttributeSet.extractAndRecycle(context: Context, arrayId: IntArray, f: TypedArray.() -> Unit) {
    context.theme.obtainStyledAttributes(
        this,
        arrayId,
        0, 0
    ).let {
        it.f()
        it.recycle()
    }
}

internal fun EditText.setTextIfNeeded(newText: String?): Boolean {
    if (newText == null) return false
    if (this.text.toString() == newText) return false
    this.setText(newText)
    return true
}

internal fun TextView.goneOnBlank() {
    this.visibility = if (this.length() <= 0) View.GONE else View.VISIBLE
}

internal fun TextView.invisibleOnBlank() {
    this.visibility = if (this.length() <= 0) View.INVISIBLE else View.VISIBLE
}

internal fun View.visible() {
    this.visibility = View.VISIBLE
}

internal fun View.gone() {
    this.visibility = View.GONE
}

internal fun View.invisible() {
    this.visibility = View.INVISIBLE
}

internal fun View.isVisible() = this.visibility == View.VISIBLE

internal fun View.isGone() = this.visibility == View.GONE

internal fun View.isInvisible() = this.visibility == View.INVISIBLE

internal fun View.setCustomSize(newWidth: Int? = null, newHeight: Int? = null) {
    layoutParams.apply {
        if (newWidth != null) this.width = newWidth
        if (newHeight != null) this.height = newHeight
    }.let { newLayoutParams ->
        layoutParams = newLayoutParams
    }
}

var longClickJob: Job? = null

@SuppressLint("ClickableViewAccessibility")
internal fun View.setShrinkOnTouchAnimation(
    shrinkPercent: Float = 0.9f,
    duration: Long = 300,
    longClickTimeout: Long = 1000
) {
    setOnTouchListener { v, event ->
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                longClickJob = GlobalScope.launch(Dispatchers.IO) {
                    delay(longClickTimeout)
                    ensureActive()
                    withContext(Dispatchers.Main) { performLongClick() }
                }
                animate().scaleX(shrinkPercent).scaleY(shrinkPercent).setDuration(duration).start()
                return@setOnTouchListener true
            }

            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                animate().scaleX(1f).scaleY(1f).setDuration(duration).start()
                if (longClickJob?.isCompleted != true) {
                    longClickJob?.cancel()
                    performClick()
                }
                return@setOnTouchListener true
            }
        }

        return@setOnTouchListener false
    }
}

internal fun TextView.setTextColorHex(textColorHex: String?, @ColorRes defaultColorResId: Int? = null) {
    textColorHex?.parseColorHex(this.context, defaultColorResId)?.let {
        setTextColor(it)
    }
}

internal fun TextView.setFontSizeSP(fontSizeInSp: Int) {
    setTextSize(TypedValue.COMPLEX_UNIT_SP, fontSizeInSp.toFloat())
}

internal fun TextView.setFontWithName(fontName: String) {
    fontName.parseFont(context).let { typeface = it }
}

internal fun String.parseColorHex(context: Context? = null, defaultColorResId: Int? = null): Int? = try {
    Color.parseColor(this)
} catch (e: Exception) {
    context?.takeIf { defaultColorResId != null }
        ?.let { ContextCompat.getColor(context, defaultColorResId!!) }
}

internal fun String.parseFont(context: Context): Typeface? {
    val fontId = when (this.lowercase().replace("-", "_")) {
        "aiaeverest_regular" -> R.font.aiaeverest_regular
        "aiaeverest_medium" -> R.font.aiaeverest_medium
        "aiaeverest_bold" -> R.font.aiaeverest_bold
        "aiaeverest_condensedmedium" -> R.font.aiaeverest_condensedmedium
        "opensans_light" -> R.font.opensans_light
        "opensans_regular" -> R.font.opensans_regular
        "opensans_semibold" -> R.font.opensans_semibold
        "opensans_bold" -> R.font.opensans_bold
        else -> null
    } ?: return null

    return ResourcesCompat.getFont(context, fontId)
}


internal fun TextView.setHtmlText(html: String?) {
    this.text = html?.let {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_LEGACY)
        } else {
            Html.fromHtml(html)
        }
    } ?: ""
}

internal fun TextView.setLinkMovementMethod() {
    movementMethod = LinkMovementMethod.getInstance()
}

internal fun ImageView.load(url: String?, customWidth: Int? = null, customHeight: Int? = null, customTransforms: List<BitmapTransformation>? = null) {
    GlideApp.with(this).apply {
        clear(this@load)
        if (url.isNullOrBlank()) return@apply
        load(url).setDefaultOptions(customWidth, customHeight, customTransforms).into(this@load)
    }
}

internal fun View.setBackgroundColorHexForDrawable(colorHex: String?) {
    colorHex?.parseColorHex()?.let {
        (background as? GradientDrawable)?.apply {
            mutate()
            setColor(it)
        }
    }
}

internal fun View.setStrokeColorHexForDrawable(colorHex: String?) {
    colorHex?.parseColorHex()?.let {
        (background as? GradientDrawable)?.apply {
            mutate()
            setStroke(resources.getDimensionPixelSize(R.dimen.default_line_height), it)
        }
    }
}

internal fun View.setBorderColorHexForDrawable(colorHex: String?, strokeWidth: Int) {
    colorHex?.parseColorHex()?.let { (background as? GradientDrawable)?.setStroke(strokeWidth, it) }
}

internal fun Context.getInputMethodManager() = getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager

internal fun EditText.hideKeyboard() {
    context.getInputMethodManager()?.hideSoftInputFromWindow(windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
}

@SuppressLint("CheckResult")
internal fun GlideRequest<Drawable>.setDefaultOptions(
    customWidth: Int? = null,
    customHeight: Int? = null,
    customTransforms: List<BitmapTransformation>? = null
): GlideRequest<Drawable> {
    centerInside()
    if (customWidth ?: 0 > 0 && customHeight ?: 0 > 0)
        override(customWidth!!, customHeight!!)
    if (!customTransforms.isNullOrEmpty())
        transform(MultiTransformation(customTransforms))
    return this
}

internal fun View.clickWithDebounce(debounceTime: Long = 500L, action: (View) -> Unit) {
    this.setOnClickListener(object : View.OnClickListener {
        private var lastClickTime: Long = 0

        override fun onClick(v: View) {
            if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
            else action(v)

            lastClickTime = SystemClock.elapsedRealtime()
        }
    })
}
