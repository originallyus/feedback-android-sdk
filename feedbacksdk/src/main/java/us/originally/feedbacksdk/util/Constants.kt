package us.originally.feedbacksdk.util

internal object Constants {
    const val TAG_INTRUSIVE_SPLASH_SCREEN = "intrusive-splash-screen"
    const val TAG_FEEDBACK_DIALOG = "feedback-dialog"
    const val TAG_USEFULNESS_CONTENT_DIALOG = "usefulness-content-dialog"
    const val KEY_RANDOM_UUID = "device-random-uuid"
    const val SLUG_INSTALL_TIMESTAMP = "install-timestamp"
    const val SLUG_SERVER_TIMESTAMP_DELTA = "server-timestamp-delta"
    const val SLUG_METADATA = "metadata"
    const val TYPE_USEFULNESS_CONTENT = "content_usefulness"
}