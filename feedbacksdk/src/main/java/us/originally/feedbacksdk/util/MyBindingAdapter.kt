package us.originally.feedbacksdk.util

import android.animation.Animator
import android.animation.ObjectAnimator
import android.util.Pair
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.databinding.BindingAdapter
import us.originally.feedbacksdk.util.extension.gone
import us.originally.feedbacksdk.util.extension.load
import us.originally.feedbacksdk.util.extension.parseColorHex
import us.originally.feedbacksdk.util.extension.setFontSizeSP
import us.originally.feedbacksdk.util.extension.setFontWithName
import us.originally.feedbacksdk.util.extension.setTextColorHex
import us.originally.feedbacksdk.util.extension.visible


@BindingAdapter("strColorFilter")
internal fun AppCompatImageView.setColorFilterString(color: String?) {
    color?.parseColorHex()?.let { setColorFilter(it) }
}

@BindingAdapter("txtColor", "txtFontSize", "txtFont")
internal fun AppCompatTextView.setTextTheme(color: String?, fontSize: Int?, font: String?) {
    setTextColorHex(color)
    fontSize?.let { setFontSizeSP(fontSize) }
    font?.let { setFontWithName(font) }
}

@BindingAdapter("loadImageUrl")
internal fun ImageView.loadImageUrl(url: String?) {
    load(url, width, height)
}

@BindingAdapter("app:layout_constraintWidth_percent")
internal fun View.setLayoutConstraintWidthPercent(width: Float) {
    (this.layoutParams as? ConstraintLayout.LayoutParams)?.apply {
        this.matchConstraintPercentWidth = width
    }
}

@BindingAdapter("animatedVisibility")
internal fun View.setAnimatedVisibility(newVisibility: Pair<Int, Long>) {
    if (this.visibility == newVisibility.first) return
    val willShow = newVisibility.first == View.VISIBLE
    val willHide = newVisibility.first == View.GONE
    this.animate().alpha(if (willShow) 1f else 0f).setDuration(newVisibility.second)
        .setListener(object : Animator.AnimatorListener {
            override fun onAnimationStart(animation: Animator) {
                if (willShow) this@setAnimatedVisibility.visible()
            }

            override fun onAnimationEnd(animation: Animator) {
                if (willHide) this@setAnimatedVisibility.gone()
            }

            override fun onAnimationCancel(animation: Animator) {
            }

            override fun onAnimationRepeat(animation: Animator) {
            }
        })
}

@BindingAdapter("animatedRotation")
internal fun View.setAnimatedRotation(rotationAnimation: Pair<Float, Long>) {
    if (this.rotation == rotationAnimation.first) return
    ObjectAnimator.ofFloat(this, View.ROTATION, rotationAnimation.first).apply {
        duration = rotationAnimation.second
    }.start()
}