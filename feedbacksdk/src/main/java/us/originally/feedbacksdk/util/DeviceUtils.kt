package us.originally.feedbacksdk.util


import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.pm.PackageManager
import android.provider.Settings
import android.telephony.TelephonyManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.core.content.ContextCompat
import com.orhanobut.hawk.Hawk
import java.util.*

/**
 * Created by originally.us on 4/15/14.
 */
internal object DeviceUtils {

    private val LOG_TAG = "DeviceUtils"

    fun hideKeyboard(activity: Activity?) {
        activity?.window?.currentFocus?.let { focusView ->
            (activity.getSystemService(Context.INPUT_METHOD_SERVICE) as? InputMethodManager)?.hideSoftInputFromWindow(
                focusView.windowToken,
                0
            )
        }
    }

    fun hideKeyboard(context: Context?, editText: EditText?) {
        if (context == null || editText == null) return
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.hideSoftInputFromWindow(editText.windowToken, 0)
    }


    fun showKeyboard(activity: Activity?) {
        val v = activity?.window?.currentFocus
        if (v != null) {
            val imm = activity.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.showSoftInput(v, InputMethodManager.SHOW_IMPLICIT)
        }
    }

    fun showKeyboard(context: Context?, editText: EditText?) {
        val imm = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm?.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT)
    }

    @SuppressLint("MissingPermission", "HardwareIds")
    fun  getDeviceUUID(context: Context?): String {
        try {
            // 1st option: Device ID
            if (context != null && ContextCompat.checkSelfPermission(
                    context,
                    Manifest.permission.READ_PHONE_STATE
                ) == PackageManager.PERMISSION_GRANTED
            ) {
                val deviceId =
                    (context.getSystemService(Context.TELEPHONY_SERVICE) as? TelephonyManager)?.deviceId
                if (!deviceId.isNullOrBlank()) return deviceId
            }

            // 2nd option: Android ID
            val androidId =
                Settings.Secure.getString(context?.contentResolver, Settings.Secure.ANDROID_ID)
            if (!androidId.isNullOrBlank())
                return androidId

            // Last option: RandomUUID
            var currentUUID = Hawk.get<String>(Constants.KEY_RANDOM_UUID)
            if (!currentUUID.isNullOrBlank())
                return currentUUID

            currentUUID = UUID.randomUUID().toString()
            Hawk.put(Constants.KEY_RANDOM_UUID, currentUUID)
            return currentUUID
        } catch (e: Exception) {
            e.printStackTrace()
            return ""
        }
    }
}
