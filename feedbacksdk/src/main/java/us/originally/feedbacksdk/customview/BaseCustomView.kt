package us.originally.feedbacksdk.customview

import android.content.Context
import android.content.res.TypedArray
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import androidx.databinding.ViewDataBinding

abstract class BaseCustomView<T : ViewDataBinding> : FrameLayout {

    open lateinit var mBinding: T

    constructor(context: Context) : super(context) {
        initialize()
    }

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        initialize(attrs)
    }

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        initialize(attrs)
    }

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes) {
        initialize(attrs)
    }

    open fun initialize(attrs: AttributeSet? = null) {
        mBinding = inflateViewBinding(LayoutInflater.from(context))
        isClickable = true
    }

    abstract fun inflateViewBinding(layoutInflater: LayoutInflater): T

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if (w > 0 && h > 0) onVisible()
    }

    open fun onVisible() {
        refreshView()
    }

    open fun refreshView() {}

    open fun AttributeSet.extractAndRecycle(
        context: Context,
        arrayId: IntArray,
        f: TypedArray.() -> Unit
    ) {
        context.theme.obtainStyledAttributes(
            this,
            arrayId,
            0, 0
        ).let {
            it.f()
            it.recycle()
        }
    }
}

