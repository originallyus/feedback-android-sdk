package us.originally.feedbacksdk.customview

import android.content.Context
import android.graphics.drawable.GradientDrawable
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.LinearLayout
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.view.children
import androidx.databinding.BindingAdapter
import androidx.databinding.DataBindingUtil
import us.originally.feedbacksdk.R
import us.originally.feedbacksdk.databinding.ViewFeedbackRatingNumberBinding
import us.originally.feedbacksdk.databinding.ViewNumberRatingItemBinding
import us.originally.feedbacksdk.model.Theme
import us.originally.feedbacksdk.util.extension.*

internal class FeedbackNumberRatingView : BaseCustomView<ViewFeedbackRatingNumberBinding> {

    companion object {
        @JvmStatic
        @BindingAdapter("numberRatingTheme", "numberRatingMin", "numberRatingMax", "numberRatingLabelLeft", "numberRatingLabelRight")
        fun FeedbackNumberRatingView.setData(theme: Theme?, ratingMin: Int?, ratingMax: Int?, labelLeft: String?, labelRight: String?) {
            mTheme = theme
            mRatingMin = ratingMin ?: 1
            mRatingMax = ratingMax ?: 5
            mLabelLeft = labelLeft
            mLabelRight = labelRight
            rebuildRatingView()
        }
    }

    var mTheme: Theme? = null

    var mLabelLeft: String? = null

    var mLabelRight: String? = null

    var mRatingMin: Int = 1

    var mRatingMax: Int = 5

    var mRatingCurrent: Int = 0

    var mOnRatingChanged: ((Int) -> Unit)? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun inflateViewBinding(layoutInflater: LayoutInflater): ViewFeedbackRatingNumberBinding =
        ViewFeedbackRatingNumberBinding.inflate(layoutInflater, this, true)

    override fun initialize(attrs: AttributeSet?) {
        super.initialize(attrs)
        rebuildRatingView()
    }

    private fun rebuildRatingView() {
        mBinding.apply {
            tvLeft.apply {
                text = mLabelLeft
                if (mLabelLeft.isNullOrBlank()) gone() else visible()
                applyLabelTheme()
            }
            tvRight.apply {
                text = mLabelRight
                if (mLabelRight.isNullOrBlank()) gone() else visible()
                applyLabelTheme()
            }
            layoutNumbers.apply {
                removeAllViews()
                val layoutInflater = LayoutInflater.from(context)
                for (rating in mRatingMin..mRatingMax) {
                    val isFirstItem = rating == mRatingMin
                    val isLastItem = rating == mRatingMax
                    val itemView = layoutInflater.inflate(R.layout.view_number_rating_item, null)
                    DataBindingUtil.bind<ViewNumberRatingItemBinding>(itemView)?.apply {
                        tvNumber.apply {
                            tag = rating
                            text = rating.toString()
                            setOnClickListener {
                                setSelectedRating(rating)
                            }
                            applyRatingTheme(isFirstItem, isLastItem)
                        }
                    }
                    this.addView(itemView, LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT).apply {
                        weight = 1f
                        marginStart = if (isFirstItem) 0 else -resources.getDimensionPixelSize(R.dimen.default_line_height)
                    })
                }
            }
        }
    }

    private fun setSelectedRating(selectedRating: Int) {
        mRatingCurrent = selectedRating
        mBinding.layoutNumbers.children.forEach {
            DataBindingUtil.bind<ViewNumberRatingItemBinding>(it)?.tvNumber?.apply {
                val itemRating = (tag as? Int) ?: -1
                isSelected = itemRating == selectedRating
                applyRatingTheme(itemRating == mRatingMin, itemRating == mRatingMax)
            }
        }
        mOnRatingChanged?.invoke(selectedRating)
    }

    private fun AppCompatTextView.applyLabelTheme() {
        setTextColorHex(mTheme?.scale_label_color, R.color.color_text_grey)
        mTheme?.scale_label_fontsize?.let { setFontSizeSP(it) }
        mTheme?.scale_label_font?.let { setFontWithName(it) }
    }

    private fun AppCompatTextView.applyRatingTheme(isFirstItem: Boolean, isLastItem: Boolean) {
        val strTextColor = if (isSelected) mTheme?.rating_number_selected_text_color else mTheme?.rating_number_text_color
        setTextColorHex(strTextColor, R.color.color_text_dark_grey)

        mTheme?.rating_number_font?.let { setFontWithName(it) }

        mTheme?.rating_number_fontsize?.let {
            setFontSizeSP(it)
        }

        val strBgColor = if (isSelected) mTheme?.rating_number_selected_bg_color else mTheme?.rating_number_bg_color
        val strBorderColor = if (isSelected) mTheme?.rating_number_selected_border_color else mTheme?.rating_number_border_color
        (background as? GradientDrawable)?.apply {
            mutate()
            this.setStroke(resources.getDimensionPixelSize(R.dimen.default_line_height), strBorderColor?.parseColorHex() ?: ContextCompat.getColor(context, R.color.line_color))
            this.setColor(strBgColor?.parseColorHex() ?: ContextCompat.getColor(context, R.color.white))
            val defaultCornerRadius = resources.getDimension(R.dimen.default_radius)
            val topLeftRadius = if (isFirstItem) defaultCornerRadius else 0f
            val bottomLeftRadius = if (isFirstItem) defaultCornerRadius else 0f
            val topRightRadius = if (isLastItem) defaultCornerRadius else 0f
            val bottomRightRadius = if (isLastItem) defaultCornerRadius else 0f
            this.cornerRadii = floatArrayOf(topLeftRadius, topLeftRadius, topRightRadius, topRightRadius, bottomRightRadius, bottomRightRadius, bottomLeftRadius, bottomLeftRadius)
        }
    }

}