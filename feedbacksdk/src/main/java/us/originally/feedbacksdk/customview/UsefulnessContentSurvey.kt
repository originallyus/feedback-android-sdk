package us.originally.feedbacksdk.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.Constraints
import androidx.core.view.updateLayoutParams
import androidx.databinding.BindingAdapter
import us.originally.feedbacksdk.BR
import us.originally.feedbacksdk.R
import us.originally.feedbacksdk.databinding.ViewUsefulnessContentSurveyBinding
import us.originally.feedbacksdk.model.Form

internal class UsefulnessContentSurvey : BaseCustomView<ViewUsefulnessContentSurveyBinding> {

    companion object {
        @JvmStatic
        @BindingAdapter("form")
        fun UsefulnessContentSurvey.setForm(form: Form?) {
            mForm = form
        }

        @JvmStatic
        @BindingAdapter("negativeButtonClick")
        fun UsefulnessContentSurvey.setNegativeButtonClick(click: OnClickListener?) {
            mNegativeClickListener = click
        }

    }

    var mForm: Form? = null
        set(value) {
            field = value
            mBinding.apply {
                form = value
                positiveButtonTheme = value?.theme
                negativeButtonTheme =
                    value?.theme?.copy(
                        button_bg_color = value.theme.button_text_color,
                        button_text_color = value.theme.button_bg_color,
                        button_stroke_color = value.theme.button_bg_color
                    )
                notifyChange()
            }

            val isIntrusiveStyle = value?.checkIntrusiveStyle() == true

            mBinding.layoutRoot.updateLayoutParams<ViewGroup.LayoutParams> {
                height = if (isIntrusiveStyle) ViewGroup.LayoutParams.MATCH_PARENT else ViewGroup.LayoutParams.WRAP_CONTENT
            }

            ConstraintSet().apply { clone(mBinding.layoutRoot) }.apply {
                // Positive button
                clear(R.id.btn_positive, ConstraintSet.TOP)
                clear(R.id.btn_positive, ConstraintSet.BOTTOM)
                if (isIntrusiveStyle) {
                    connect(R.id.btn_positive, ConstraintSet.BOTTOM, R.id.btn_negative, ConstraintSet.TOP)
                } else {
                    connect(R.id.btn_positive, ConstraintSet.TOP, R.id.tv_question, ConstraintSet.BOTTOM)
                }

                // Negative button
                clear(R.id.btn_negative, ConstraintSet.TOP)
                clear(R.id.btn_negative, ConstraintSet.BOTTOM)
                if (isIntrusiveStyle)
                    connect(R.id.btn_negative, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
                else
                    connect(R.id.btn_negative, ConstraintSet.TOP, R.id.btn_positive, ConstraintSet.BOTTOM)
            }.applyTo(mBinding.layoutRoot)

            val spaceMediumMargin = resources.getDimensionPixelSize(R.dimen.space_m)
            val spaceLargeMargin = resources.getDimensionPixelSize(R.dimen.space_xl)
            mBinding.btnPositive.updateLayoutParams<ConstraintLayout.LayoutParams> {
                setMargins(0, if (isIntrusiveStyle) 0 else spaceLargeMargin, 0, spaceMediumMargin)
            }
            mBinding.btnNegative.updateLayoutParams<ConstraintLayout.LayoutParams> {
                setMargins(0, if (isIntrusiveStyle) 0 else spaceMediumMargin, 0, 0)
            }
        }

    var mPositiveClickListener: OnClickListener? = null
        set(value) {
            field = value
            mBinding.apply {
                positiveClickListener = value
                notifyPropertyChanged(BR.positiveClickListener)
            }
        }

    var mNegativeClickListener: OnClickListener? = null
        set(value) {
            field = value
            mBinding.apply {
                negativeClickListener = value
                notifyPropertyChanged(BR.negativeClickListener)
            }
        }

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun inflateViewBinding(layoutInflater: LayoutInflater): ViewUsefulnessContentSurveyBinding =
        ViewUsefulnessContentSurveyBinding.inflate(layoutInflater, this, true)

    override fun initialize(attrs: AttributeSet?) {
        super.initialize(attrs)
        clipChildren = false
        clipToPadding = false
    }
}