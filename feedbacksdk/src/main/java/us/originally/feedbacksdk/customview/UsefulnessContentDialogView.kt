package us.originally.feedbacksdk.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.view.updateLayoutParams
import us.originally.feedbacksdk.R
import us.originally.feedbacksdk.databinding.ViewUsefulnessContentDialogBinding
import us.originally.feedbacksdk.model.Form
import us.originally.feedbacksdk.util.extension.setBlinkClickAnimation

internal class UsefulnessContentDialogView : BaseCustomView<ViewUsefulnessContentDialogBinding> {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun inflateViewBinding(layoutInflater: LayoutInflater): ViewUsefulnessContentDialogBinding =
        ViewUsefulnessContentDialogBinding.inflate(layoutInflater, this, true)

    var mForm: Form? = null
        set(value) {
            field = value
            mBinding.apply {
                form = value
                notifyChange()
            }

            val isIntrusiveStyle = value?.checkIntrusiveStyle() == true

            // Update survey margins
            mBinding.viewSurvey.updateLayoutParams<ConstraintLayout.LayoutParams> {
                height = if (isIntrusiveStyle) 0 else ConstraintLayout.LayoutParams.WRAP_CONTENT
                val spaceM = resources.getDimensionPixelSize(R.dimen.space_m)
                val spaceL = resources.getDimensionPixelSize(R.dimen.space_l)
                setMargins(spaceL, spaceM, spaceL, 0)
            }

            // Update survey top/bottom constrains
            ConstraintSet().apply { clone(mBinding.layoutRoot) }.apply {
                clear(R.id.view_survey, ConstraintSet.TOP)
                clear(R.id.view_survey, ConstraintSet.BOTTOM)
                clear(R.id.view_bottom_space, ConstraintSet.TOP)
                clear(R.id.view_bottom_space, ConstraintSet.BOTTOM)

                connect(R.id.view_survey, ConstraintSet.TOP, R.id.btn_close, ConstraintSet.BOTTOM)
                if (isIntrusiveStyle) {
                    connect(R.id.view_bottom_space, ConstraintSet.BOTTOM, ConstraintSet.PARENT_ID, ConstraintSet.BOTTOM)
                    connect(R.id.view_survey, ConstraintSet.BOTTOM, R.id.view_bottom_space, ConstraintSet.TOP)
                } else {
                    connect(R.id.view_bottom_space, ConstraintSet.TOP, R.id.view_survey, ConstraintSet.BOTTOM)
                }
            }.applyTo(mBinding.layoutRoot)
        }

    var mOnCloseClick: (() -> Unit)? = null

    var mPositiveClick: (() -> Unit)? = null

    var mNegativeClick: (() -> Unit)? = null

    override fun initialize(attrs: AttributeSet?) {
        super.initialize(attrs)

        mBinding.apply {
            btnClose.setBlinkClickAnimation().setOnClickListener {
                mOnCloseClick?.invoke()
            }
            viewSurvey.apply {
                mPositiveClickListener = OnClickListener {
                    this@UsefulnessContentDialogView.mPositiveClick?.invoke()
                }
                mNegativeClickListener = OnClickListener {
                    this@UsefulnessContentDialogView.mNegativeClick?.invoke()
                }
            }
        }
    }
}