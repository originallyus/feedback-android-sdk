package us.originally.feedbacksdk.customview

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.drawable.Drawable
import android.graphics.drawable.LayerDrawable
import android.util.AttributeSet
import android.util.Pair
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.RatingBar
import android.widget.TextView
import androidx.annotation.ColorInt
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.graphics.drawable.DrawableCompat
import androidx.core.widget.addTextChangedListener
import com.google.android.material.shape.CornerFamily
import timber.log.Timber
import us.originally.feedbacksdk.BR
import us.originally.feedbacksdk.R
import us.originally.feedbacksdk.databinding.ViewFeedbackFormContentBinding
import us.originally.feedbacksdk.model.Form
import us.originally.feedbacksdk.util.extension.*

internal class FeedbackFormContentView : BaseCustomView<ViewFeedbackFormContentBinding> {

    interface Listener {
        fun onLaunchUrlClick(url: String)

        fun onCloseDialogClick()

        fun onShowOriginalFormClick()

        fun onGoToStoreReviewClick(storeUrl: String)

        fun onSubmitClick(
            starRating: Int? = null,
            numberRating: Int? = null,
            selectedOptions: String? = null,
            comment: String? = null,
            isAutoSubmit: Boolean
        )
    }

    var mIsSubmitting: Boolean = false
        set(value) {
            field = value
            mBinding.apply {
                isSubmitting = value
                notifyPropertyChanged(BR.isSubmitting)
            }
        }

    var mIsCollapsed: Boolean = false
        set(value) {
            if (!mInTabletMode) return
            field = value
            mBinding.apply {
                isCollapsed = value
                notifyPropertyChanged(BR.isCollapsed)
            }

        }

    var mIsSubmittedForm: Boolean = false

    var mIsUsingExtraForm: Boolean = false

    var mInTabletMode: Boolean = false
        set(value) {
            field = value
            mBinding.apply {
                inTabletMode = value
                notifyPropertyChanged(BR.inTabletMode)
            }
        }

    var mData: Form? = null
        set(value) {
            field = value

            mBinding.apply {
                val formTheme = value?.theme
                val isIntrusiveStyle = value?.instrusive_style == 1

                // Set card corners
                val cardCorner = resources.getDimension(R.dimen.default_radius)
                cardView.shapeAppearanceModel = cardView.shapeAppearanceModel.toBuilder()
                    .setTopLeftCorner(CornerFamily.ROUNDED, cardCorner)
                    .setTopRightCorner(CornerFamily.ROUNDED, cardCorner)
                    .apply {
                        if (mInTabletMode && isIntrusiveStyle) {
                            setBottomLeftCorner(CornerFamily.ROUNDED, cardCorner)
                            setBottomRightCorner(CornerFamily.ROUNDED, cardCorner)
                        } else {
                            setBottomRightCornerSize(0f)
                            setBottomLeftCornerSize(0f)
                        }
                    }
                    .build()

                // Set content size base on form style
                scrollView.apply {
                    (layoutParams as? ConstraintLayout.LayoutParams)?.apply {
                        height =
                            if (isIntrusiveStyle && !mInTabletMode) 0 else ViewGroup.LayoutParams.WRAP_CONTENT
                        ConstraintSet().apply { clone(mBinding.layoutRoot) }.apply {
                            // Set height
                            if (isIntrusiveStyle && !mInTabletMode) {
                                clear(R.id.scroll_view, ConstraintSet.TOP)
                                connect(
                                    R.id.scroll_view,
                                    ConstraintSet.TOP,
                                    R.id.layout_header,
                                    ConstraintSet.BOTTOM
                                )
                            }

                            // Set width
                            constrainPercentWidth(
                                R.id.scroll_view,
                                if (mInTabletMode && isIntrusiveStyle) 0.8f else 1f
                            )
                        }.applyTo(mBinding.layoutRoot)
                    }
                }

                // Button margin
                if (mInTabletMode && isIntrusiveStyle)
                    (btnAction.layoutParams as? ConstraintLayout.LayoutParams)?.apply {
                        bottomMargin = resources.getDimensionPixelSize(R.dimen.space_l)
                    }?.let {
                        btnAction.layoutParams = it
                    }

                // Intrusive
                intrusiveStyle = isIntrusiveStyle

                // Theme
                theme = formTheme

                // Alignment
                isLeftAlignment = value?.checkIfLeftAlignment() == true

                // Background color
                formTheme?.body_bg_color?.parseColorHex()?.let {
                    cardView.setCardBackgroundColor(it)
                }

                // Header title
                tvTitleHeader.applyTextTheme(
                    formTheme?.title_color,
                    formTheme?.title_fontsize,
                    formTheme?.title_font
                )

                // Close icon
                formTheme?.close_icon_color?.parseColorHex()?.let { closeIconColor ->
                    mBinding.btnClose.setColorFilter(closeIconColor)
                }

                // Image
                imageFileUrl = value?.image_file_url

                // Title
                title = value?.title
                tvTitle.applyTextTheme(
                    formTheme?.title_color,
                    formTheme?.title_fontsize,
                    formTheme?.title_font
                )

                // Question
                question = value?.question
                tvQuestion.applyTextTheme(
                    formTheme?.question_color,
                    formTheme?.question_fontsize,
                    formTheme?.question_font
                )

                // Rating star
                ratingBar.applyCustomStarColor()
                ratingBar.apply {
                    rating = value?.read_only_rating?.toFloat() ?: 0f
                    setIsIndicator(value?.read_only_rating ?: 0 > 0)
                }

                // Rating number
                ratingMin = value?.rating_min ?: 1
                ratingMax = value?.rating_max ?: 5
                scaleLabelLeft = value?.scale_label_left
                scaleLabelRight = value?.scale_label_right

                // Instruction
                val strInstruction =
                    getStringWithRating(value?.instruction_5_star, value?.instruction)
                instruction = strInstruction
                tvInstruction.applyTextTheme(
                    formTheme?.instruction_color,
                    formTheme?.instruction_fontsize,
                    formTheme?.instruction_font
                )

                // Options
                options = value?.options
                optionsAllowMultiSelect = value?.checkMultiSelectOptions() == true

                // 2nd Question
                val strQuestion2 =
                    getStringWithRating(value?.secondary_question_5_star, value?.secondary_question)
                question2 = strQuestion2
                tvQuestion2.applyTextTheme(
                    formTheme?.secondary_question_color,
                    formTheme?.secondary_question_fontsize,
                    formTheme?.secondary_question_font
                )

                // Comment
                commentPlaceholder = getStringWithRating(
                    value?.comment_placeholder_5_star,
                    value?.comment_placeholder
                )
                edtComment.applyEditTextTheme(
                    formTheme?.textarea_text_color,
                    formTheme?.textarea_fontsize,
                    formTheme?.textarea_font,
                    formTheme?.textarea_border_color,
                    formTheme?.textarea_focus_border_color,
                    formTheme?.textarea_bg_color,
                    formTheme?.textarea_focus_bg_color,
                    formTheme?.textarea_placeholder_color
                )

                // Fineprint
                val strFineprint = value?.fineprint
                fineprint = strFineprint
                tvFineprint.applyTextTheme(
                    formTheme?.fineprint_color,
                    formTheme?.fineprint_fontsize,
                    formTheme?.fineprint_font
                )

                // Submit button
                buttonText = value?.button_text
                checkButtonAvailable()

                notifyChange()
            }
        }

    private val mCurrentRatingStar: Int
        get() = mBinding.ratingBar.rating.toInt()

    private val mCurrentRatingNumber: Int
        get() = mBinding.viewNumberRating.mRatingCurrent

    var mListener: Listener? = null

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    )

    constructor(
        context: Context,
        attrs: AttributeSet?,
        defStyleAttr: Int,
        defStyleRes: Int
    ) : super(context, attrs, defStyleAttr, defStyleRes)

    override fun inflateViewBinding(layoutInflater: LayoutInflater): ViewFeedbackFormContentBinding =
        ViewFeedbackFormContentBinding.inflate(layoutInflater, this, true)

    private fun checkCurrent5StarRating() =
        mData?.checkShouldShowRatingStar() == true && mBinding.ratingBar.rating == 5f

    private fun getStringWithRating(strFiveStar: String?, strOther: String?) =
        if (checkCurrent5StarRating()) strFiveStar
            ?: strOther else strOther

    @SuppressLint("ClickableViewAccessibility")
    override fun initialize(attrs: AttributeSet?) {
        super.initialize(attrs)

        mBinding.apply {
            layoutHeader.setOnClickListener {
                if (!mIsCollapsed) return@setOnClickListener
                mIsCollapsed = !mIsCollapsed
                reloadViewsVisibility(true)
            }

            viewNumberRating.mOnRatingChanged = { newRating ->
                hideCommentKeyboard()
                reloadViewsVisibility(true)
                checkAutoSubmitRating(newRating)
            }

            viewFeedbackOptions.mOnSelectedOptionsChanged = {
                hideCommentKeyboard()
                reloadViewsVisibility(true)
            }

            ratingBar.setOnRatingBarChangeListener { _, rating, fromUser ->
                if (fromUser) {
                    hideCommentKeyboard()

                    // Update texts if any
                    mBinding.apply {
                        instruction =
                            getStringWithRating(mData?.instruction_5_star, mData?.instruction)
                        question2 = getStringWithRating(
                            mData?.secondary_question_5_star,
                            mData?.secondary_question
                        )
                        commentPlaceholder = getStringWithRating(
                            mData?.comment_placeholder_5_star,
                            mData?.comment_placeholder
                        )
                        notifyPropertyChanged(BR.instruction)
                        notifyPropertyChanged(BR.question2)
                        notifyPropertyChanged(BR.commentPlaceholder)
                    }

                    // Update views visibility
                    reloadViewsVisibility(true)

                    // Check auto submit
                    checkAutoSubmitRating(rating.toInt())
                }
            }

            edtComment.apply {
                addTextChangedListener {
                    checkButtonAvailable()
                }
            }

            btnAction.setBlinkClickAnimation().clickWithDebounce {
                hideCommentKeyboard()
                onSubmitClick(false)
            }

            btnClose.setBlinkClickAnimation().setOnClickListener {
                hideCommentKeyboard()

                if (mInTabletMode && mData?.checkIntrusiveStyle() != true && !mIsSubmittedForm) {
                    mIsCollapsed = !mIsCollapsed
                    reloadViewsVisibility(true)
                    return@setOnClickListener
                }

                mListener?.onCloseDialogClick()
            }

            setupHideKeyboardOnTouchOutside(
                layoutRoot,
                layoutContent,
                imvImage,
                tvTitle,
                tvInstruction,
                tvQuestion,
                tvQuestion2,
                tvFineprint,
                btnAction,
                btnClose,
                ratingBar,
                viewNumberRating,
                viewFeedbackOptions
            )
        }
    }

    private fun hideCommentKeyboard() {
        mBinding.edtComment.apply {
            hideKeyboard()
            clearFocus()
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    private fun setupHideKeyboardOnTouchOutside(vararg views: View) {
        views.forEach {
            it.setOnTouchListener { v, event ->
                if (event.action == MotionEvent.ACTION_DOWN)
                    hideCommentKeyboard()
                return@setOnTouchListener false
            }
        }
    }

    private fun onSubmitClick(isAutoSubmit: Boolean) {
        // Launch url if any
        if (!mData?.url.isNullOrBlank()) {
            mListener?.onLaunchUrlClick(mData?.url!!)
        }

        // Submit form data
        if (!mIsSubmittedForm) {
            mListener?.onSubmitClick(
                starRating = if (mData?.checkShouldShowRatingStar() == true) mCurrentRatingStar else null,
                numberRating = if (mData?.checkShouldShowRatingNumber() == true) mCurrentRatingNumber else null,
                selectedOptions = mBinding.viewFeedbackOptions.mSelectedSlugs?.joinToString(
                    separator = ","
                ),
                comment = mBinding.edtComment.text?.toString(),
                isAutoSubmit = isAutoSubmit
            )
            return
        }

        // Go to store review case
        if (mData?.checkNeedAutoLaunchStoreReview() != true && !mData?.android_review_url.isNullOrBlank()) {
            mData?.android_review_url?.let { mListener?.onGoToStoreReviewClick(it) }
            return
        }

        // Show original form after extra form
        if (mIsUsingExtraForm) {
            mListener?.onShowOriginalFormClick()
            return
        }

        mListener?.onCloseDialogClick()
    }

    private fun checkAutoSubmitRating(currentRating: Int) {
        if (mData?.auto_submit?.contains(currentRating) == true)
            onSubmitClick(true)
    }

    private fun isSelectedAnyRatingStar() = mCurrentRatingStar > 0

    private fun isSelectedAnyRatingNumber() = mCurrentRatingNumber > 0

    private fun isSelectedAnyOption() = !mBinding.viewFeedbackOptions.mSelectedSlugs.isNullOrEmpty()

    private fun checkButtonAvailable() {
        mBinding.apply {
            allowButtonClick = when {
                mData?.show_comment == true && mData?.comment_mandatory == 1 && mBinding.edtComment.text?.toString()
                    .isNullOrBlank() -> false

                else -> true
            }
            notifyPropertyChanged(BR.allowButtonClick)
        }
    }

    private fun Int.getAnimationDuration(withAnimation: Boolean) = when {
        !withAnimation -> 0L
        this == VISIBLE -> 300L
        else -> 100L
    }

    fun reloadViewsVisibility(withAnimation: Boolean) {
        Timber.d("reloadViewsVisibility: withAnimation=$withAnimation")
        val shouldShowRatingStar = mData?.checkShouldShowRatingStar() == true
        val shouldShowRatingNumber = mData?.checkShouldShowRatingNumber() == true
        val hasOptionsData = !mData?.options.isNullOrEmpty()

        val shouldShowAllViews =
            (!shouldShowRatingStar && !shouldShowRatingNumber && !hasOptionsData)
                    || (shouldShowRatingStar && isSelectedAnyRatingStar())
                    || (shouldShowRatingNumber && isSelectedAnyRatingNumber())
                    || (hasOptionsData && isSelectedAnyOption())
        var newVisibility = 0

        mBinding.apply {
            // Close button
            val rotateDegree = if (mIsCollapsed) 45f else 0f
            val rotateDuration = if (!withAnimation) 0L else 150L
            closeButtonRotation = Pair(rotateDegree, rotateDuration)
            notifyPropertyChanged(BR.closeButtonRotation)

            // Image
            newVisibility = if (!mData?.image_file_url.isNullOrBlank()) VISIBLE else GONE
            imageVisibility = Pair(newVisibility, newVisibility.getAnimationDuration(withAnimation))
            notifyPropertyChanged(BR.imageVisibility)

            // Question
            newVisibility = if (!mData?.question.isNullOrBlank()) VISIBLE else GONE
            questionVisibility =
                Pair(newVisibility, newVisibility.getAnimationDuration(withAnimation))
            notifyPropertyChanged(BR.questionVisibility)

            // Instruction
            val strInstruction = getStringWithRating(mData?.instruction_5_star, mData?.instruction)
            val shouldShowInstruction =
                !strInstruction.isNullOrBlank() && (shouldShowAllViews || (!shouldShowRatingStar && !shouldShowRatingNumber && hasOptionsData))
            newVisibility = if (shouldShowInstruction) VISIBLE else GONE
            instructionVisibility =
                Pair(newVisibility, newVisibility.getAnimationDuration(withAnimation))
            notifyPropertyChanged(BR.instructionVisibility)

            // Star rating
            newVisibility = if (shouldShowRatingStar) VISIBLE else GONE
            starRatingVisibility =
                Pair(newVisibility, newVisibility.getAnimationDuration(withAnimation))
            notifyPropertyChanged(BR.starRatingVisibility)

            // Number rating
            newVisibility = if (shouldShowRatingNumber) VISIBLE else GONE
            numberRatingVisibility =
                Pair(newVisibility, newVisibility.getAnimationDuration(withAnimation))
            notifyPropertyChanged(BR.numberRatingVisibility)

            // Question 2
            val strQuestion2 =
                getStringWithRating(mData?.secondary_question_5_star, mData?.secondary_question)
            val shouldShowQuestion2 = !strQuestion2.isNullOrBlank() && shouldShowAllViews
            newVisibility = if (shouldShowQuestion2) VISIBLE else GONE
            question2Visibility =
                Pair(newVisibility, newVisibility.getAnimationDuration(withAnimation))
            notifyPropertyChanged(BR.question2Visibility)

            // Options
            val shouldShowOptions =
                hasOptionsData && ((shouldShowRatingStar && isSelectedAnyRatingStar()) || (shouldShowRatingNumber && isSelectedAnyRatingNumber()) || (!shouldShowRatingNumber && !shouldShowRatingStar))
            newVisibility = if (shouldShowOptions) VISIBLE else GONE
            optionsVisibility =
                Pair(newVisibility, newVisibility.getAnimationDuration(withAnimation))
            notifyPropertyChanged(BR.optionsVisibility)

            // Comment
            val shouldShowComment = mData?.show_comment == true && shouldShowAllViews
            newVisibility = if (shouldShowComment) VISIBLE else GONE
            commentVisibility =
                Pair(newVisibility, newVisibility.getAnimationDuration(withAnimation))
            notifyPropertyChanged(BR.commentVisibility)

            // Fineprint
            val shouldShowFineprint = !mData?.fineprint.isNullOrBlank() && shouldShowAllViews
            newVisibility = if (shouldShowFineprint) VISIBLE else GONE
            fineprintVisibility =
                Pair(newVisibility, newVisibility.getAnimationDuration(withAnimation))
            notifyPropertyChanged(BR.fineprintVisibility)

            // Button
            val shouldShowButton =
                !mData?.button_text.isNullOrBlank() && shouldShowAllViews && !mIsCollapsed
            newVisibility = if (shouldShowButton) VISIBLE else GONE
            buttonVisibility =
                Pair(newVisibility, newVisibility.getAnimationDuration(withAnimation))
            notifyPropertyChanged(BR.buttonVisibility)
        }
    }

    private fun EditText.applyEditTextTheme(
        textColorHex: String? = null,
        fontSize: Int? = null,
        font: String? = null,
        borderColorHex: String? = null,
        focusBorderColorHex: String? = null,
        backgroundColorHex: String? = null,
        focusBackgroundColorHex: String? = null,
        placeHolderColorHex: String? = null
    ) {
        setTextColorHex(textColorHex)
        fontSize?.let { setFontSizeSP(fontSize) }
        font?.let { setFontWithName(font) }
        placeHolderColorHex?.parseColorHex()?.let { setHintTextColor(it) }
        setBackgroundColorHexForDrawable(backgroundColorHex)
        setBorderColorHexForDrawable(
            borderColorHex,
            resources.getDimensionPixelSize(R.dimen.default_line_height)
        )
        setOnFocusChangeListener { v, hasFocus ->
            this.setBackgroundColorHexForDrawable(if (hasFocus) focusBackgroundColorHex else backgroundColorHex)
            this.setBorderColorHexForDrawable(
                if (hasFocus) focusBorderColorHex else borderColorHex,
                resources.getDimensionPixelSize(R.dimen.default_line_height)
            )
        }
    }

    private fun TextView.applyTextTheme(
        textColorHex: String? = null,
        fontSize: Int? = null,
        font: String? = null
    ) {
        setTextColorHex(textColorHex)
        fontSize?.let { setFontSizeSP(fontSize) }
        font?.let { setFontWithName(font) }
    }

    private fun RatingBar.applyCustomStarColor() {
        (this.progressDrawable as? LayerDrawable)?.let { stars ->
            stars.mutate()

            // Filled stars
            mData?.theme?.selected_star_color?.parseColorHex()?.let {
                setRatingStarColor(stars.getDrawable(2), it)
            }

            mData?.theme?.normal_star_color?.parseColorHex()?.let {
                // Half filled stars
                setRatingStarColor(stars.getDrawable(1), it)

                // Empty stars
                setRatingStarColor(stars.getDrawable(0), it)
            }
        }
    }

    private fun setRatingStarColor(
        drawable: Drawable,
        @ColorInt color: Int
    ) {
        DrawableCompat.setTint(drawable, color)
    }
}