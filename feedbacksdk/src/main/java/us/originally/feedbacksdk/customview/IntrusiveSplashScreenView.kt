package us.originally.feedbacksdk.customview

import android.content.Context
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import us.originally.feedbacksdk.databinding.ViewIntrusiveSplashScreenBinding
import us.originally.feedbacksdk.model.Form
import us.originally.feedbacksdk.util.extension.parseColorHex
import us.originally.feedbacksdk.util.extension.setBlinkClickAnimation

internal class IntrusiveSplashScreenView : BaseCustomView<ViewIntrusiveSplashScreenBinding> {

    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)
    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int, defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    var mData: Form? = null
        set(value) {
            field = value
            mBinding.apply {
                title = value?.splash_screen_title
                subtitle = value?.splash_screen_question
                buttonProceed = value?.splash_screen_button_proceed
                buttonSkip = value?.splash_screen_button_skip
                closeIconColor = value?.theme?.close_icon_color
                theme = value?.theme
                buttonProceedTheme = value?.theme
                buttonSkipTheme = value?.theme?.copy(button_bg_color = value.theme.button_text_color, button_text_color = value.theme.button_bg_color, button_stroke_color = value.theme.button_bg_color)
                notifyChange()
            }
        }

    var mOnCloseClick: (() -> Unit)? = null

    var mOnProceedClick: (() -> Unit)? = null

    var mOnSkipClick: (() -> Unit)? = null

    override fun inflateViewBinding(layoutInflater: LayoutInflater): ViewIntrusiveSplashScreenBinding =
        ViewIntrusiveSplashScreenBinding.inflate(layoutInflater, this, true)

    override fun initialize(attrs: AttributeSet?) {
        super.initialize(attrs)

        mBinding.apply {
            btnProceed.setBlinkClickAnimation().setOnClickListener {
                mOnProceedClick?.invoke()
            }
            btnSkip.setBlinkClickAnimation().setOnClickListener {
                mOnSkipClick?.invoke()
            }
            btnClose.setBlinkClickAnimation().setOnClickListener {
                mOnCloseClick?.invoke()
            }
        }
    }
}