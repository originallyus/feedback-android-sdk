package us.originally.feedbacksdk

import android.animation.Animator
import android.annotation.SuppressLint
import android.app.Activity
import android.app.Application
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.FrameLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.orhanobut.hawk.Hawk
import kotlinx.coroutines.*
import timber.log.Timber
import us.originally.feedbacksdk.api.ApiManager
import us.originally.feedbacksdk.api.ApiResult
import us.originally.feedbacksdk.customview.FeedbackFormContentView
import us.originally.feedbacksdk.customview.IntrusiveSplashScreenView
import us.originally.feedbacksdk.customview.UsefulnessContentDialogView
import us.originally.feedbacksdk.manager.InAppReviewManager
import us.originally.feedbacksdk.model.Form
import us.originally.feedbacksdk.util.CacheUtils
import us.originally.feedbacksdk.util.Constants
import us.originally.feedbacksdk.util.GlideApp
import us.originally.feedbacksdk.util.ToastUtils
import us.originally.feedbacksdk.util.extension.*
import java.util.concurrent.TimeUnit

@SuppressLint("StaticFieldLeak")
object FeedbackSdk : Application.ActivityLifecycleCallbacks {

    internal var mAppPackageName: String? = null

    internal var mAppSecret: String? = null

    internal var mCurrentFeedbackDialogData: Form? = null

    private var mRequestFormJob: Job? = null

    private var mSubmitFormJob: Job? = null

    private const val SLIDE_ANIMATION_DURATION = 300L

    fun init(application: Application, appSecret: String) {
        mAppPackageName = application.packageName
        mAppSecret = appSecret
        application.registerActivityLifecycleCallbacks(this)
        Hawk.init(application.applicationContext).build()
        Timber.plant(Timber.DebugTree())
        CacheUtils.saveDeviceUUID(application.applicationContext)
    }

    fun setLanguage(languageCode: String) {
        CacheUtils.saveLanguageCode(languageCode)
    }

    fun getCurrentLanguage() = CacheUtils.getLanguageCode()

    fun setUserId(userId: String?) {
        CacheUtils.saveUserId(userId)
    }

    fun getCurrentUserId() = CacheUtils.getUserId()

    fun setMetadata(metadata: String?) {
        CacheUtils.saveObject(Constants.SLUG_METADATA, metadata)
    }

    fun getCurrentMetadata() = CacheUtils.getObject<String>(Constants.SLUG_METADATA)

    /*
     * Forcefully dismiss the rating UI
     * This may be suitable for scenarios like session timeout, session expired where host application
     * needs to forcefully dismiss any user-related UI and logout the user
     * Note: This is not part of the initial UI/UX design and business requirement. This will interfere with the
     * showing and prompting frequency logics in the backend. Please consult your business team before deciding to use this
     */
    fun forceDismiss(activity: Activity) {
        closeFloatingView(activity, Constants.TAG_INTRUSIVE_SPLASH_SCREEN, true)
        closeFloatingView(activity, Constants.TAG_FEEDBACK_DIALOG, true)
        closeFloatingView(activity, Constants.TAG_USEFULNESS_CONTENT_DIALOG, true)
    }

    fun showFeedbackDialog(
        activity: Activity, formSlug: String, eventTag: String?, debug: Boolean
    ) {
        // Check exiting usefulness content view
        if (activity.getContentViewGroup()?.findViewWithTag<View>(Constants.TAG_USEFULNESS_CONTENT_DIALOG) != null) return
        // Check exiting intrusive splash screen view
        if (activity.getContentViewGroup()?.findViewWithTag<View>(Constants.TAG_INTRUSIVE_SPLASH_SCREEN) != null) return
        // Check exiting dialog view
        if (activity.getContentViewGroup()?.findViewWithTag<View>(Constants.TAG_FEEDBACK_DIALOG) != null) return
        // Check exiting request form
        if (mRequestFormJob?.isActive == true) return
        val activityScope = (activity as? AppCompatActivity)?.lifecycleScope
        mRequestFormJob = activityScope?.launch {
            when (val result = ApiManager.requestForm(formSlug, if (debug) 1 else 0, eventTag)) {
                is ApiResult.Success -> {
                    mCurrentFeedbackDialogData = result.data ?: return@launch

                    if (mCurrentFeedbackDialogData?.title.isNullOrBlank() || mCurrentFeedbackDialogData?.theme == null) return@launch

                    // Show splash screen if any
                    if (!mCurrentFeedbackDialogData?.splash_screen_title.isNullOrBlank()) {
                        showIntrusiveSplashScreen(activity, mCurrentFeedbackDialogData!!, formSlug, eventTag, debug)
                        return@launch
                    }

                    // Show usefulness content
                    if (mCurrentFeedbackDialogData?.type == Constants.TYPE_USEFULNESS_CONTENT) {
                        showUsefulnessContentDialog(activity, mCurrentFeedbackDialogData!!, formSlug, eventTag, debug)
                        return@launch
                    }

                    // Show feedback content
                    showFeedbackDialogContent(activity, mCurrentFeedbackDialogData!!, formSlug, eventTag, debug)
                }

                is ApiResult.Failure -> {
                    Timber.e(result.error)
                    if (debug) ToastUtils.showError(activity, result.error.message)
                }

                else -> {}
            }
        }
    }

    private fun showUsefulnessContentDialog(activity: Activity, data: Form, formSlug: String, eventTag: String?, debug: Boolean) {
        val isIntrusiveStyle = data.checkIntrusiveStyle()
        val inTabletMode = activity.isThisATablet()

        val usefulnessContentView = UsefulnessContentDialogView(activity).apply {
            id = View.generateViewId()
            tag = Constants.TAG_USEFULNESS_CONTENT_DIALOG
            mForm = data
            mOnCloseClick = {
                closeFloatingView(activity, Constants.TAG_USEFULNESS_CONTENT_DIALOG)
            }
            mPositiveClick = {
                closeFloatingView(activity, Constants.TAG_USEFULNESS_CONTENT_DIALOG, true)

                // Submit form
                submitUsefulnessContentSelectedOption(activity, data, 2, eventTag, debug)
            }
            mNegativeClick = {
                closeFloatingView(activity, Constants.TAG_USEFULNESS_CONTENT_DIALOG, true)

                // Submit form
                submitUsefulnessContentSelectedOption(activity, data, 1, eventTag, debug)
            }
        }

        // Add
        if (inTabletMode) {
            val dialogWidth = (activity.getScreenWidth()
                .toFloat() * (if (isIntrusiveStyle) 0.6f else 0.3f)).toInt()
            activity.addContentView(usefulnessContentView,
                FrameLayout.LayoutParams(
                    dialogWidth,
                    FrameLayout.LayoutParams.WRAP_CONTENT
                ).apply {
                    gravity = if (isIntrusiveStyle) Gravity.CENTER else Gravity.BOTTOM
                })
        } else {
            activity.addContentView(usefulnessContentView,
                FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT, if (isIntrusiveStyle)
                        FrameLayout.LayoutParams.MATCH_PARENT
                    else
                        FrameLayout.LayoutParams.WRAP_CONTENT
                ).apply {
                    gravity = Gravity.BOTTOM
                })
        }

        // Show with slide animation
        usefulnessContentView.afterMeasured {
            // Hide
            animate().translationY(this.height.toFloat()).setDuration(0).start()
            // Show
            animate().translationY(0f).setDuration(SLIDE_ANIMATION_DURATION).start()
        }
    }

    private fun submitUsefulnessContentSelectedOption(activity: Activity, form: Form, selectedOption: Int, eventTag: String?, debug: Boolean) {
        val activityScope = (activity as? AppCompatActivity)?.lifecycleScope ?: return
        val formId = form.id ?: return
        val formSlug = form.slug ?: return

        activityScope.launch {
            // Check delay seconds
            if ((form.delay_second ?: 0) > 0)
                delay(TimeUnit.SECONDS.toMillis(form.delay_second!!.toLong()))

            mSubmitFormJob = launch {
                val submitResult = ApiManager.submitForm(
                    formId,
                    formSlug,
                    rating = null,
                    selectedOptions = selectedOption.toString(),
                    freeText = null,
                    if (debug) 1 else 0,
                    false,
                    eventTag
                )
                if (mSubmitFormJob?.isActive != true) return@launch
                when (submitResult) {
                    is ApiResult.Success -> {
                        // Set submit result
                        mCurrentFeedbackDialogData = submitResult.data ?: return@launch

                        // Show splash screen if any
                        if (!mCurrentFeedbackDialogData?.splash_screen_title.isNullOrBlank()) {
                            showIntrusiveSplashScreen(activity, mCurrentFeedbackDialogData!!, formSlug, eventTag, debug)
                            return@launch
                        }

                        // Show feedback content
                        showFeedbackDialogContent(activity, mCurrentFeedbackDialogData!!, formSlug, eventTag, debug)
                    }

                    is ApiResult.Failure -> {
                        Timber.e(submitResult.error)
                        ToastUtils.showError(
                            activity,
                            submitResult.error.message
                        )
                    }

                    else -> {}
                }
            }
        }
    }

    private fun showIntrusiveSplashScreen(activity: Activity, data: Form, formSlug: String, eventTag: String?, debug: Boolean) {
        val splashScreenView = IntrusiveSplashScreenView(activity).apply {
            id = View.generateViewId()
            tag = Constants.TAG_INTRUSIVE_SPLASH_SCREEN
            mData = data
            mOnSkipClick = {
                closeFloatingView(activity, Constants.TAG_INTRUSIVE_SPLASH_SCREEN)
            }
            mOnProceedClick = {
                closeFloatingView(activity, Constants.TAG_INTRUSIVE_SPLASH_SCREEN, true)
                showFeedbackDialogContent(activity, data, formSlug, eventTag, debug)
            }
            mOnCloseClick = {
                closeFloatingView(activity, Constants.TAG_INTRUSIVE_SPLASH_SCREEN)
            }
        }

        // Add view
        activity.addContentView(
            splashScreenView,
            FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.MATCH_PARENT)
        )

        // Show with slide animation
        splashScreenView.afterMeasured {
            // Hide
            animate().translationY(this.height.toFloat()).setDuration(0).start()
            // Show
            animate().translationY(0f).setDuration(SLIDE_ANIMATION_DURATION).start()
        }
    }

    private fun showFeedbackDialogContent(activity: Activity, data: Form, formSlug: String, eventTag: String?, debug: Boolean) {
        // Build
        val isIntrusiveStyle = data?.checkIntrusiveStyle() == true
        val inTabletMode = activity.isThisATablet()
        val activityScope = (activity as? AppCompatActivity)?.lifecycleScope
        val feedbackFormContentView = FeedbackFormContentView(activity).apply {
            id = View.generateViewId()
            tag = Constants.TAG_FEEDBACK_DIALOG
            mIsSubmittedForm = false
            mIsUsingExtraForm = false
            mInTabletMode = inTabletMode
            mIsCollapsed = inTabletMode && !isIntrusiveStyle
            mIsSubmitting = false
            mData = data
            mListener = object : FeedbackFormContentView.Listener {
                override fun onLaunchUrlClick(url: String) {
                    url.openUrlWithCustomTabs(activity)
                }

                override fun onCloseDialogClick() {
                    mSubmitFormJob?.cancel()
                    closeFloatingView(activity, Constants.TAG_FEEDBACK_DIALOG)
                }

                override fun onShowOriginalFormClick() {
                    this@apply.mIsUsingExtraForm = false
                    this@apply.mData = mCurrentFeedbackDialogData
                    reloadViewsVisibility(true)
                }

                override fun onGoToStoreReviewClick(storeUrl: String) {
                    if (this@apply.mIsUsingExtraForm) {
                        this@apply.apply {
                            mIsUsingExtraForm = false
                            mData = mCurrentFeedbackDialogData
                            reloadViewsVisibility(true)
                        }
                    } else
                        closeFloatingView(activity, Constants.TAG_FEEDBACK_DIALOG)

                    try {
                        activity.startActivity(
                            Intent(
                                Intent.ACTION_VIEW,
                                Uri.parse(storeUrl)
                            )
                        )
                    } catch (e: Exception) {
                        Timber.e(e)
                        ToastUtils.showError(context, e.message)
                        return
                    }
                }

                override fun onSubmitClick(
                    starRating: Int?,
                    numberRating: Int?,
                    selectedOptions: String?,
                    comment: String?,
                    isAutoSubmit: Boolean
                ) {
                    val formId = mCurrentFeedbackDialogData?.id ?: return
                    val rating = when {
                        mCurrentFeedbackDialogData?.checkShouldShowRatingStar() == true -> starRating
                            ?: 0

                        mCurrentFeedbackDialogData?.checkShouldShowRatingNumber() == true -> numberRating
                            ?: 0

                        else -> 0
                    }
                    mSubmitFormJob = activityScope?.launch {
                        this@apply.mIsSubmitting = true
                        val submitResult = ApiManager.submitForm(
                            formId,
                            formSlug,
                            rating,
                            selectedOptions,
                            comment,
                            if (debug) 1 else 0,
                            isAutoSubmit,
                            eventTag
                        )
                        if (mSubmitFormJob?.isActive != true) return@launch
                        when (submitResult) {
                            is ApiResult.Success -> {
                                // Set submit result
                                mCurrentFeedbackDialogData = submitResult.data
                                this@apply.apply {
                                    mIsSubmitting = false
                                    mIsSubmittedForm = true
                                    mIsUsingExtraForm =
                                        submitResult.data?.extra_form != null
                                    mData =
                                        if (mIsUsingExtraForm) submitResult.data?.extra_form else submitResult.data
                                    reloadViewsVisibility(true)
                                }

                                // Show auto rating if needed
                                if (submitResult.data?.checkNeedAutoLaunchStoreReview() == true) {
                                    InAppReviewManager.startInAppReviewFlow(activity, mAppPackageName)
                                }
                            }

                            is ApiResult.Failure -> {
                                this@apply.mIsSubmitting = false
                                Timber.e(submitResult.error)
                                ToastUtils.showError(
                                    activity,
                                    submitResult.error.message
                                )
                            }

                            else -> {}
                        }
                    }
                }
            }
            reloadViewsVisibility(false)
        }

        // Add
        if (inTabletMode) {
            val dialogWidth = (activity.getScreenWidth()
                .toFloat() * (if (isIntrusiveStyle) 0.6f else 0.3f)).toInt()
            activity.addContentView(feedbackFormContentView,
                FrameLayout.LayoutParams(
                    dialogWidth,
                    FrameLayout.LayoutParams.WRAP_CONTENT
                ).apply {
                    gravity = if (isIntrusiveStyle) Gravity.CENTER else Gravity.BOTTOM
                })
        } else {
            activity.addContentView(feedbackFormContentView,
                FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.MATCH_PARENT, if (isIntrusiveStyle)
                        FrameLayout.LayoutParams.MATCH_PARENT
                    else
                        FrameLayout.LayoutParams.WRAP_CONTENT
                ).apply {
                    gravity = Gravity.BOTTOM
                })
        }

        // Show
        feedbackFormContentView.afterMeasured {
            this.animate().translationY(
                getOffscreenPosition(context, this.height, inTabletMode, isIntrusiveStyle)
            ).apply {
                // Set tablet non-intrusive x position
                if (inTabletMode && !isIntrusiveStyle) {
                    val newX = (activity.getScreenWidth()
                        .toFloat() * 0.75f) - (this@afterMeasured.width.toFloat() / 2f)
                    translationX(newX)
                }
            }.setDuration(0).start()
            this.animate().translationY(0f).setDuration(SLIDE_ANIMATION_DURATION)
                .start()
        }

        // Preload images if any
        val preloadImages = mutableListOf<String>().apply {
            data?.image_file_url?.let { add(it) }
            addAll(data?.preload_images ?: emptyList())
        }
        preloadImages.forEach { imageUrl ->
            activityScope?.launch(Dispatchers.IO) {
                GlideApp.with(activity).load(imageUrl)
                    .listener(object : RequestListener<Drawable> {
                        override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                        ): Boolean {
                            return false
                        }

                        override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                        ): Boolean {
                            Timber.d("Preloaded: $imageUrl")
                            return false
                        }
                    }).preload()
            }
        }
    }

    private fun getOffscreenPosition(
        context: Context,
        dialogHeight: Int,
        inTabletMode: Boolean,
        isIntrusiveStyle: Boolean
    ): Float {
        val screenHeight = context.getScreenHeight()
        var offscreenPosition = dialogHeight.toFloat()
        if (inTabletMode && isIntrusiveStyle) {
            offscreenPosition += (screenHeight - dialogHeight) / 2f
        }
        return offscreenPosition
    }

    private fun closeFloatingView(activity: Activity, tag: String, immediately: Boolean = false) {
        val dialogViewToClose = activity.getContentViewGroup()?.findViewWithTag<View>(tag) ?: return
        if (immediately)
            activity.getContentViewGroup()?.removeView(dialogViewToClose)
        else
            dialogViewToClose.run {
                animate().translationY(
                    getOffscreenPosition(
                        context,
                        this.height,
                        context.isThisATablet(),
                        mCurrentFeedbackDialogData?.checkIntrusiveStyle() == true
                    )
                ).setDuration(SLIDE_ANIMATION_DURATION)
                    .setListener(object : Animator.AnimatorListener {

                        override fun onAnimationStart(p0: Animator) {
                        }

                        override fun onAnimationEnd(p0: Animator) {
                            activity.getContentViewGroup()?.removeView(this@run)
                        }

                        override fun onAnimationCancel(p0: Animator) {
                        }

                        override fun onAnimationRepeat(p0: Animator) {
                        }
                    })
                    .start()
            }
    }

    private fun Activity.getContentViewGroup(): ViewGroup? = this.findViewById(android.R.id.content)

    override fun onActivityPaused(activity: Activity) {
    }

    override fun onActivityStarted(activity: Activity) {
    }

    override fun onActivityDestroyed(activity: Activity) {
        closeFloatingView(activity, Constants.TAG_INTRUSIVE_SPLASH_SCREEN, true)
        closeFloatingView(activity, Constants.TAG_FEEDBACK_DIALOG, true)
        closeFloatingView(activity, Constants.TAG_USEFULNESS_CONTENT_DIALOG, true)
    }

    override fun onActivitySaveInstanceState(activity: Activity, p1: Bundle) {
    }

    override fun onActivityStopped(activity: Activity) {
    }

    override fun onActivityCreated(activity: Activity, p1: Bundle?) {
    }

    override fun onActivityResumed(activity: Activity) {
    }
}