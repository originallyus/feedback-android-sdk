package us.originally.feedbacksdk.manager

import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Build
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory
import timber.log.Timber
import java.util.*

internal object InAppReviewManager {

    private lateinit var mReviewManager: ReviewManager

    fun startInAppReviewFlow(activity: Activity, packageName: String?) {
        if (!this::mReviewManager.isInitialized)
            mReviewManager = ReviewManagerFactory.create(activity)

        mReviewManager.requestReviewFlow()
            .addOnSuccessListener { reviewInfo ->
                mReviewManager.launchReviewFlow(activity, reviewInfo)
            }
            .addOnFailureListener {
                Timber.e(it)
                if (packageName.isNullOrBlank()) return@addOnFailureListener
                launchAppStore(activity, packageName)
            }
    }

    fun launchAppStore(context: Context, packageName: String) {
        try {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=$packageName")))
        } catch (e: ActivityNotFoundException) {
            val manufacturer = Build.MANUFACTURER
            if (manufacturer.equals("huawei", true) && launchExternalAppStore(context, "appmarket", packageName)) return
            if (manufacturer.equals("xiaomi", true) && launchExternalAppStore(context, "mimarket", packageName)) return
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=$packageName")))
        }
    }

    private fun launchExternalAppStore(context: Context, storeScheme: String, packageName: String): Boolean {
        return try {
            context.startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("$storeScheme://details?id=$packageName")))
            true
        } catch (e: ActivityNotFoundException) {
            Timber.e(e)
            false
        }
    }
}