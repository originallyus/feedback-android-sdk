package us.originally.feedbacksdk.api

import com.google.gson.Gson
import com.google.gson.JsonElement
import us.originally.feedbacksdk.manager.DateTimeManager
import java.lang.reflect.Type

internal object ApiResultHandler {

    fun <T> handleSuccess(data: JsonElement, type: Type, gson: Gson): ApiResult<T> {
        return ApiResult.success<T>(
                try {
                    // Save server timestamp
                    if (data.isJsonObject && data.asJsonObject.has("timestamp")) {
                        DateTimeManager.saveServerTimestamp(data.asJsonObject["timestamp"].asLong)
                    }

                    gson.fromJson<T>(data, type)
                } catch (e: Exception) {
                    e.printStackTrace()
                    null
                }
        )
    }

    fun <T> handleException(e: Exception): ApiResult<T> {
        return ApiResult.failure<T>(e)
    }

}