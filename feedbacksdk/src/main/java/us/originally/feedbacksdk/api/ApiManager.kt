package us.originally.feedbacksdk.api

import android.os.Build
import us.originally.feedbacksdk.BuildConfig
import us.originally.feedbacksdk.FeedbackSdk
import us.originally.feedbacksdk.model.Form
import us.originally.feedbacksdk.util.CacheUtils
import us.originally.feedbacksdk.util.Constants
import java.util.concurrent.TimeUnit

internal object ApiManager : BaseApiManager() {

    override suspend fun getStandardHeaders(): HashMap<String, String> =
        hashMapOf<String, String>().apply {
            put("X-Device-Uuid", CacheUtils.getDeviceUUID())
            put("X-OS", "android")
            put("X-Sdk-Platform", "android")
            put("X-Version", "0.3.0")
            put("X-OS-Version", Build.VERSION.RELEASE)
            put("X-App-Version", BuildConfig.VERSION_NAME)
            put("X-Build-Number", BuildConfig.VERSION_CODE.toString())
            put("X-App-Secret", FeedbackSdk.mAppSecret ?: "")
            put("X-Package-Id", FeedbackSdk.mAppPackageName ?: "")
            put("X-App-User-Id", CacheUtils.getUserId())
            put("X-Lang", CacheUtils.getLanguageCode())
        }

    override suspend fun getStandardParams(): HashMap<String, String> = hashMapOf(
        "install_timestamp" to "${CacheUtils.getObject<Long>(Constants.SLUG_INSTALL_TIMESTAMP) ?: 0}"
    ).also {
        if (CacheUtils.getObject<Long>(Constants.SLUG_INSTALL_TIMESTAMP) == null)
            CacheUtils.saveObject(
                Constants.SLUG_INSTALL_TIMESTAMP,
                TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis())
            )
    }

    suspend fun requestForm(slug: String, debug: Int, eventTag: String?) = postGeneric<Form>(
        "8vs2qwFD1rlSFWIa13Wc",
        hashMapOf<String, String>(),
        hashMapOf("slug" to slug, "debug" to debug.toString()).apply {
            eventTag?.let { put("event_tag", it) }
        }.toRequestJson(), Form::class.java
    )

    suspend fun submitForm(
        formId: Long,
        slug: String,
        rating: Int? = null,
        selectedOptions: String? = null,
        freeText: String? = null,
        debug: Int,
        auto: Boolean,
        eventTag: String?
    ) = postGeneric<Form>(
        "LXvcFSTTOqxxmJX9qQar",
        hashMapOf<String, String>(),
        hashMapOf(
            "request_id" to formId.toString(),
            "slug" to slug,
            "debug" to debug.toString(),
            "auto" to if (auto) "1" else "0",
        ).apply {
            eventTag?.let { put("event_tag", it) }
            if (!selectedOptions.isNullOrBlank())
                put("selected_options", selectedOptions)
            if (!freeText.isNullOrBlank())
                put("free_text", freeText)
            if (rating != null)
                put("rating", rating.toString())
        }.toRequestJson(), Form::class.java
    )
}