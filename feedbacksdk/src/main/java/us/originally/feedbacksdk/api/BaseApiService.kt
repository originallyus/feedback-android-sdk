package us.originally.feedbacksdk.api

import com.google.gson.GsonBuilder
import com.google.gson.JsonElement
import okhttp3.CertificatePinner
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import java.util.concurrent.TimeUnit

@JvmSuppressWildcards
internal interface BaseApiService {

    companion object {
        private const val PRODUCTION_URL = "https://aia-dfs.originally.us/backend/"
        const val GENERIC_URL = "{url}"

        fun create(customBaseUrl: String = ""): BaseApiService {
            val headers = getHeader()
            val gson = GsonBuilder().apply {
                setDateFormat("yyyy-MM-dd")
            }.create()

            return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(getBaseUrl(customBaseUrl))
                .client(headers)
                .build().create(BaseApiService::class.java)
        }

        private fun getBaseUrl(customBaseUrl: String = ""): String {
            if (customBaseUrl.isNotBlank()) return customBaseUrl
            return PRODUCTION_URL
        }

        private fun getHeader(): OkHttpClient {
            val builder = OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .certificatePinner(CertificatePinner.Builder().apply {
                    add("originally.us", "sha256/Y9mvm0exBk1JoQ57f9Vm28jKo5lFm/woKcVxrYxu80o=")
                }.build())
                .addInterceptor(HttpLoggingInterceptor().apply {
                    setLevel(HttpLoggingInterceptor.Level.BODY)
                })
            return builder.build()
        }
    }

    @GET(GENERIC_URL)
    suspend fun getGeneric(
        @Path(value = "url", encoded = true) url: String,
        @HeaderMap headers: Map<String, String> = HashMap(),
        @QueryMap params: Map<String, String> = HashMap()
    ): JsonElement

    @POST(GENERIC_URL)
    suspend fun postGeneric(
        @Path(value = "url", encoded = true) url: String,
        @HeaderMap headers: Map<String, String> = HashMap(),
        @Body params: JsonElement
    ): JsonElement
}