package us.originally.feedbacksdk.api

internal sealed class ApiResult<T> {

    data class Success<T>(var data: T?) : ApiResult<T>()

    data class Failure<T>(val error: Throwable) : ApiResult<T>()

    class Loading<T>() : ApiResult<T>()

    companion object {

        fun <T> success(data: T?): ApiResult<T> = Success(data)

        fun <T> failure(e: Throwable): ApiResult<T> = Failure(e)

        fun <T> loading(): ApiResult<T> = Loading()
    }
}