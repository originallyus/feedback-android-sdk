package us.originally.feedbacksdksample.feature.demo

import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import us.originally.feedbacksdk.BuildConfig
import us.originally.feedbacksdk.FeedbackSdk
import us.originally.feedbacksdksample.R
import us.originally.feedbacksdksample.databinding.ActivityMainBinding
import us.originally.feedbacksdksample.databinding.DialogNumberPickerBinding
import us.originally.feedbacksdksample.feature.base.BaseActivity
import us.originally.feedbacksdksample.util.Constants

class DemoActivity : BaseActivity<ActivityMainBinding>() {

    private val mLanguageTitles by lazy {
        resources.getStringArray(R.array.lang_names)
    }

    private val mLanguageCodes by lazy {
        resources.getStringArray(R.array.lang_codes)
    }

    override fun inflateViewBinding(): ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        mBinding?.run {
            btnChangeLanguage.setOnClickListener {
                showLanguagePickerDialog()
            }

            btnSetUserId.setOnClickListener {
                FeedbackSdk.setUserId("123456810")
            }

            btnSetMetadata.setOnClickListener {
                FeedbackSdk.setMetadata("policy_2371518")
            }

            btnSatisfaction.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_SATISFACTION)
            }

            btnNps.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_NPS)
            }

            btnPoll.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_POLL)
            }

            btnEffort.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_EFFORT)
            }

            btnComment.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_COMMENT)
            }

            btnExternal.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_EXTERNAL)
            }

            btnSatisfactionProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_SATISFACTION)
            }

            btnNpsProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_NPS)
            }

            btnPollProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_POLL)
            }

            btnCommentProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_COMMENT)
            }

            btnEffortProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_EFFORT)
            }

            btnExternalProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_EXTERNAL)
            }

            btnContentUsefulness.setOnClickListener {
                debugFeedbackDialog(Constants.SLUG_CONTENT_USEFULNESS)
            }

            btnContentUsefulnessProduct.setOnClickListener {
                showFeedbackDialog(Constants.SLUG_CONTENT_USEFULNESS)
            }

            tvVersion.text = "Version ${BuildConfig.VERSION_NAME}"
        }
    }

    private fun debugFeedbackDialog(slug: String, eventTag: String? = null) {
        //This will always show the rating form in DEBUG mode
        //Optional: eventTag is optional
        FeedbackSdk.showFeedbackDialog(this, slug, eventTag, true)

        /* Sample code for force dismissing UI, to be used only for session timeout or session expired scenarios */
        /*
         * Forcefully dismiss the rating UI
         * This may be suitable for scenarios like session timeout, session expired where host application
         * needs to forcefully dismiss any user-related UI and logout the user
         * Note: This is not part of the initial UI/UX design and business requirement. This will interfere with the
         * showing and prompting frequency logics in the backend. Please consult your business team before deciding to use this
         */
        /*
        FeedbackSdk.forceDismiss(this);
         */
    }

    private fun showFeedbackDialog(slug: String, eventTag: String? = null) {
        //A form may not be shown all the time, depends on the frequency configurations on our SDK backend.
        //Optional: eventTag is optional
        FeedbackSdk.showFeedbackDialog(this, slug, eventTag, false)
    }

    private fun showLanguagePickerDialog() {
        val dialogBinding = DialogNumberPickerBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this)
        builder.setView(dialogBinding.root)
        dialogBinding.picker.apply {
            maxValue = mLanguageTitles.size - 1
            minValue = 0
            value = mLanguageCodes.indexOf(FeedbackSdk.getCurrentLanguage()).let { if (it < 0) 0 else it }
            displayedValues = mLanguageTitles
        }
        builder.setPositiveButton(android.R.string.ok) { dialog, which ->
            dialog.dismiss()
            mLanguageCodes.getOrNull(dialogBinding.picker.value)?.let { code ->
                //Optional: set language
                //Note: this will requires translation texts to be already available on our backend
                //Language codes: "en", "zh", "zh_tw", "ms", "id", "ta", "th", "tl", "vi"
                FeedbackSdk.setLanguage(code)
            }
        }
        builder.create().show()
    }
}
