package us.originally.feedbacksdksample

import android.app.Application
import us.originally.feedbacksdk.FeedbackSdk
import us.originally.feedbacksdksample.util.Constants

class DemoApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        //Initialize FeedbackSdk
        //Please consult your business team to obtain App Secret specific to your app's Bundle ID for different environments
        FeedbackSdk.init(this, Constants.DEMO_APP_SECRET)
    }
}